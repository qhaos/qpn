#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <string.h>
#include <qpn/qpn.h>

const uint8_t nargs[] = {
  [OP_NOTSET]   0,
  [OP_BOOL]     1,
  [OP_NOT]      1,
  [OP_BNOT]     1,
  [OP_AND]      2,
  [OP_BAND]     2,
  [OP_OR]       2,
  [OP_BOR]      2,
  [OP_XOR]      2,
  [OP_BXOR]     2,
  [OP_SHL]      2,
  [OP_SHR]      2,
  [OP_ADD]      2,
  [OP_SUB]      2,
  [OP_MUL]      2,
  [OP_DIV]      2,
  [OP_MOD]      2,
  [OP_GT]       2,
  [OP_LT]       2,
  [OP_EQ]       2,
  [OP_ASGN]     2,
  [OP_IF]       3,
  [OP_GRAB]     1,
  [OP_JMP]      2,
  [OP_PRT1]     1,
  [OP_PRT2]     2,
  [OP_PRT3]     3,
  [OP_INVALID]  0,
};


static enum op atoop(char*o) {
  switch(*o) {
    case ':':
      return OP_ASGN;
    case '!':
      return OP_NOT;
    case '~':
      return OP_BNOT;
    case '&':
      if(o[1] == '&')
        return OP_AND;
      return OP_BAND;
    case '|':
      if(o[1] == '|')
        return OP_OR;
      return OP_BOR;
    case '^':
      if(o[1] == '^')
        return OP_XOR;
      return OP_BXOR;
    case '+':
      return OP_ADD;
    case '-':
      if(o[1] == '>')
        return OP_GRAB;
      else if(o[1]=='-' && o[2]=='>')
        return OP_JMP;
      return OP_SUB;
    case '/':
      return OP_DIV;
    case '*':
      return OP_MUL;
    case '%':
      return OP_MOD;
    case '>':
      if(o[1] == '>')
        return OP_SHR;
      return OP_GT;
    case '<':
      if(o[1] == '<')
        return OP_SHL;
      return OP_LT;
    case '?':
      return OP_BOOL;
    case '\'':
      return (o[1] == '\'')?((o[2]=='\'')? OP_PRT3 : OP_PRT2): OP_PRT1;
    case '@':
      return OP_IF;
  }
  return OP_INVALID;
}

static val_t apply(qpn_state* q, const qpn_expr_t* e) {
  switch(e->op) {
    case OP_BOOL:
      return !!e->$[0];
    case OP_NOT:
      return !e->$[0];
    case OP_BNOT:
      return ~(e->$[0]);
    case OP_AND:
      return e->$[0] && e->$[1];
    case OP_BAND:
      return e->$[0] & e->$[1];
    case OP_OR:
      return e->$[0] || e->$[1];
    case OP_BOR:
      return e->$[0] | e->$[1];
    case OP_XOR:
      return !!e->$[0] ^ !!e->$[1];
    case OP_BXOR:
      return e->$[0] ^ e->$[1];
    case OP_SHL:
      return e->$[0] << e->$[1];
    case OP_SHR:
      return e->$[0] >> e->$[1];
    case OP_ADD:
      return e->$[0] + e->$[1];
    case OP_SUB:
      return e->$[0] - e->$[1];
    case OP_MUL:
      return e->$[0] * e->$[1];
    case OP_DIV:
      return e->$[0] / e->$[1];
    case OP_MOD:
      return e->$[0] % e->$[1];
    case OP_GT:
      return e->$[0] > e->$[1];
    case OP_LT:
      return e->$[0] < e->$[1];
    case OP_EQ:
      return e->$[0] == e->$[1];
    case OP_ASGN:
      return (q->var['z'-e->$[0]] = e->$[1]);
    case OP_IF:
      return e->$[0]? e->$[1] : e->$[2];
    case OP_JMP:
      return e->$[1];
    case OP_PRT3:
      printf("%lld %lld %lld\n", e->$[0], e->$[1], e->$[2]);
      return 0;
    case OP_PRT2:
      printf("%lld %lld\n", e->$[0], e->$[1]);
      return 0;
    case OP_PRT1:
      printf("%lld\n", e->$[0]);
      return 0;
  }
  printf("Invalid OP %d\n", e->op);
  q->error = true;
  return -1;
}

val_t qpn_eval(qpn_state* q) {
  qpn_expr_t e = {OP_NOTSET,0,{0,0,0}};
  for(;q->wordno < q->nwords[q->cs]; q->wordno++) {
    char c = q->words[q->cs][q->wordno][0];
    if(isdigit(c)) {
      e.$[e.f] = atoll(q->words[q->cs][q->wordno]);
      e.f++;
    } else if(isalpha(c)) {
      if(e.op == OP_ASGN && e.f == 0) {
        e.$[0] = c;
      } else {
        e.$[e.f] = q->var['z'-c];
      }
      e.f++;
    } else {
      if(e.op == OP_NOTSET) {
        e.f = 0;
        e.op = atoop(q->words[q->cs][q->wordno]);
        if(e.op == OP_INVALID) {
          if(q->dbg)
            printf("statement %d, word %d, invalid operator: %s (%x)\n", q->cs, q->wordno, q->words[q->cs][q->wordno], c);
          q->error = true;
          return -1;
        }
      } else {
        e.$[e.f] = qpn_eval(q);
        e.f++;
      }
    }
    if(e.f == nargs[e.op]) {

      val_t ret = apply(q,&e);

      if(ret&&e.op == OP_GRAB) {
        int save = q->cs;
        q-> cs = e.$[0];
        ret = qpn_eval(q);
        q->cs = save+1;
        return ret;
      }
      if(ret&&e.op==OP_JMP) {
        q->wordno = 0;
        q->cs = e.$[0];
        return qpn_eval(q);
      }
      if(q->wordno == q->nwords[q->cs]-1) {
        q->wordno = 0;
        q->cs++;
      }
      return ret;
    }
  }
  if(q->dbg) {
    /* If there is a segfault, then you will know which value is the first to go,
     * If the segfault does not trigger here, then the output looks like:
     *        Stmt: n  Word: n of n "(word)" (hex of first char)
     */
    fprintf(stderr,"Stmt: %u ", q->cs+1);
    fprintf(stderr,"Word: %i ", q->wordno+1);
    fprintf(stderr, "of %u ", q->nwords[q->cs]);
    fprintf(stderr,"\"%s\"", q->words[q->cs][q->wordno]);
    fprintf(stderr," (%x)\n", q->words[q->cs][q->wordno][0]);
  }

  q->error = true;
  return -1;
}


size_t qpn_parse(qpn_state* q, char* buff, size_t len) {
  // comments
  for(size_t i = 0; i < len; i++) {
    if(buff[i] == '[') {
      while(buff[i] != ']') {
        buff[i++]=' ';
      }
      buff[i]=' ';
    }
  }

  if(isspace(*buff)) {
    *buff = 0;
    while(isspace(*++buff))len--;
  }

  q->nstmts = 0;
  q->words[q->nstmts] = malloc(sizeof(char*) * 20);
  q->words[q->nstmts][0] = buff;

  /*
   * Give each statement pointer an address to the start of a statement
   * and replace each period with a space so it gets pruned out later
   */

  for(size_t i = 0; buff[i]; i++) {
    if(buff[i] == '.') {
      buff[i++] = 0;

      if(isspace(buff[i])) {
        while(isspace(buff[++i]));
      }
      q->words[++(q->nstmts)] = malloc(sizeof(char*) * 20);
      q->words[(q->nstmts)][0] = &buff[i];
    }
  }

  /*
   * Tokenize each word, remove whitespace and make everything in
   * between a null term so you can iterate through easily (words
   * are strings)
   */

  for(size_t i = 0; i < q->nstmts; i++) {
    q->nwords[i] = 1;
    buff = q->words[i][q->nwords[i]-1];
    for(int n = 1; buff[n]; n++) {
      if(isspace(buff[n])) {
        while(isspace(buff[n]))
          buff[n++] = 0;
        q->words[i][(++(q->nwords[i]))-1] = &buff[n];
      }
    }

  /*
   * Helps with catching parser errors, I might make debug
   * an enum for if I don't care about this stuff.
   */
    if(q->dbg) {
      printf("Statement %d has %d words:\n", i+1, q->nwords[i]);
      for(int j = 0; j < q->nwords[i]; j++)
        printf("\tword %d: %s\n", j+1, q->words[i][j]);
    }
  }

  return q->nstmts;
}

qpn_state* qpn_create(bool dbg) {
  qpn_state* q = malloc(sizeof(struct qpn_state));
  bzero(q, sizeof(*q));
  q->words = malloc(sizeof(char**)*256);
  q->nwords = malloc(sizeof(size_t)*256);
  q->dbg = dbg;
  q->error = false;
  q->done = false;
  q->nstmts = 0;
}



void qpn_destroy(qpn_state* q) {
  free(q->nwords);
  for(size_t i = 0; i < q->nstmts+1; i++) {
    free(q->words[i]);
  }
  free(q->words);
  free(q);
}
