#pragma once
#include <stdint.h>
#include <stdbool.h>
#include "ops.h"

#define QPN_BUFF_SIZE 1024*20

typedef int64_t val_t;

typedef struct qpn_expr {
  char op;
  char f;
  val_t $[3];
} qpn_expr_t;

typedef struct qpn_state {
  char*** words;
  size_t* nwords;
  size_t nstmts;
  size_t wordno;
  size_t cs;
  val_t var['z'-'A'];
  bool dbg;
  bool error;
  bool done;
} qpn_state;

val_t qpn_eval(qpn_state* q);
size_t qpn_parse(qpn_state* q, char* buff, size_t len);
qpn_state* qpn_create(bool dbg);
void qpn_destroy(qpn_state* q);
