# QPN
### a simple Polish Notation Calculator

An example of a calculation using this:

```
+ 2 * 2 / 16 4.
```
(in infix notation: 2 + 2 * (16/4))

Also, variables are available to set with the `:` operator,
Variables can only be 1 char (any character not taken by an operator), although this may change soon, an example:
```
+ : a / 49 78 - a 2.
```
would be the same as this snippet of C:
```c
int64_t a = 49 / 78;
return a + a - 2;
```

Whitespace is mostly free, with the exception that values & variables must be seperated from operators, so:
```
+
  a
  -
    2
    9
.
```
is valid, as well as
```
+ a - 2 9.
```
, and basically everything in between.

Comments are done using the [....] syntax

## Table of operators and their meanings
(trust `ops.h` over this)

| Name | Symbol | # of args | description |
| ---- | ------ | --------- | ----------- |
|  `OP_BOOL`  | `?`   | 1 | if x = 0, then return 0 otherwise, 1 | 
|  `OP_NOT`   | `!`   | 1 | logical negation of x (inverse of `?` |
|  `OP_BNOT`  | `~`   | 1 | bitwise negation of x (flip bits) |
|  `OP_AND`   | `&&`  | 2 | logical and of x and y |
|  `OP_BAND`  | `&`   | 2 | bitwise and of x and y |
|  `OP_OR`    | `||`  | 2 | logical or of x and y |
|  `OP_BOR`   | `|`   | 2 | bitwise or of x and y |
|  `OP_XOR`   | `^^`  | 2 | logical xor of x and y |
|  `OP_BXOR`  | `^`   | 2 | bitwise xor of x and y |
|  `OP_SHL`   | `<<`  | 2 | x bitshifted *left* by y bits |
|  `OP_SHR`   | `>>`  | 2 | x bitshifted *right* by y bits |
|  `OP_ADD`   | `+`   | 2 | addition of x and y |
|  `OP_SUB`   | `-`   | 2 | subtration of x and y |
|  `OP_MUL`   | `*`   | 2 | multiplication of x and y |
|  `OP_DIV`   | `/`   | 2 | division of x and y |
|  `OP_MOD`   | `%`   | 2 | modulous (C style, so sign stays) of x and y |
|  `OP_GT`    | `>`   | 2 | if x is greater than y, 1, else 0 |
|  `OP_LT`    | `<`   | 2 | if x is less than y, 1, else 0 |
|  `OP_EQ`    | `=`   | 2 | if x is equal to y, 1, else 0 |
|  `OP_ASGN`  | `:`   | 2 | variable (named by x) is set to y |
|  `OP_IF`    | `@`   | 3 | return y if x, otherwise z |
|  `OP_GRAB`  | `->`  | 1 | return value returned by statement number x (first statement is 0, etc) |
|  `OP_JMP`   | `-->` | 2 | if y, jmp to statement x |
|  `OP_PRT1`  | `'`   | 1 | print x, return zero |
|  `OP_PRT2`  | `''`  | 2 | print x, y, return zero |
|  `OP_PRT3`  | `'''` | 3 | print x, y, z, return zero |

All variables are currently initlized to 0, so a program that prints every number 1 - 20 looks like this:
```
[ This is 
 a comment ]
[stmt 0.]
: a + a 1.
' a.
--> 0 < a 20.
```

which can be written as:
```
' : a + a 1.
--> 0 < a 20.
```

## Reading the code:
(I didn't leave many comments, since no one will ever read this xD)

`main(int argc, char** argv)` - checks command line args, reads input, then tokenizes it and starts the execution loop

`val_t eval(char** words, size_t nwords)` - interprets each "word" (0..nwords-1) recursively  
Basic execution:  
```
Eval:
 1. For word in words:  
  a. if word is number:  
      I. set last unset arg to word  
  b. else if operator is unset  
      I. set operator to word  
  c. else  
      I. eval(left(words))  
 2. if number of set args is number of args for operator  
  a. return apply(operator, args...)  
```
