#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <qpn/qpn.h>

void usage() {
  puts("Options:");
  puts("  -h: prints this message and exits");
  puts("  -v: prints version and info (note: I don't actually know how to maintain a project so version might not mean jack");
  puts("  -d: turns on Debug mode (every statment's return value is put to stderr");
  puts("  -e: interpret next arguement ");
}

int main(int argc, char** argv) {
  static char _buff[4096];
  char* buff = _buff;
  size_t part = 4096;
  size_t len = 0;
  bool debug = false;
  bool cmd = false;
  int fd = 0;
  // set flags and look for filename, may transition to getopt()
  for(int i = 1; i < argc; i++) {
    if(argv[i][0] == '-' && argv[i][1]) {
      for(int j = 1; j < strlen(argv[i]); j++) {
        switch(argv[i][j]) {
          case 'v':
            puts("._________________________________________________,");
            puts("|  QPN - QH4OS Polish Notation Calculator v0.1.1  |");
            puts("|  Copyright (c) Qh4os <qh4os@protonmail.com>     |");
            puts("L_________________________________________________|");
            exit(0);
          break;
          case 'h':
            usage();
            exit(0);
          case 'd':
            debug = true;
          break;
          case 'e':
            cmd = true;
            if(i+1 == argc) {
              puts("missing program to interpret");
              exit(1);
            }
            buff = argv[i+1];
          break;
          default:
            printf("unrecognized option -%c\n", argv[i][j]);
            usage();
            exit(1);
          }
      }
    } else {
      if(!cmd) {
        fd = open(argv[i], O_RDONLY);
        if(fd < 0) {
          fprintf(stderr, "Error opening file: %s\n", argv[i]);
          exit(1);
        }
      }
    }
  }

  if(!cmd) {
    do
      len = read(fd, buff+=len, part-=len);
    while(len);

    len = QPN_BUFF_SIZE-part;
    buff = _buff;
  }

  // sha-bang!
  if(fd && buff[0] == '#') {
    while(*buff != '\n')buff++, len--;
  }


  struct qpn_state* q = qpn_create(debug);

  size_t ns = qpn_parse(q, buff, len);
  for(;q->cs < q->nstmts;) {
    val_t ret = qpn_eval(q);
    if(debug)
      fprintf(stderr, "(%lld)\n", ret);
    if(q->error) {
      puts("ERROR!");
      //for(size_t j = 0; j < argn[cs]; j++)
      //  fprintf(stderr, "args[%d][%d] = \"%s\"\n", cs, j, args[cs][j]);
      //return 1;
    }
  }

  qpn_destroy(q);

}

